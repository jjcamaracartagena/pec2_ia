
using UnityEngine;

public class FlockManager : MonoBehaviour {

    public static FlockManager ControladorFlock;
    public GameObject beePrefab;
    public int numBee = 20;
    public GameObject[] abejas;

    [HideInInspector] public Vector3 rangoLimite = new Vector3(5.0f, 5.0f, 5.0f);
    [HideInInspector] public Vector3 colmenaPos;


    void Start() {

        // Se instancian las abejas en una posición aleatoria
        abejas = new GameObject[numBee];
        for (int i = 0; i < numBee; ++i) {
            Vector3 pos = this.transform.position + new Vector3( Random.Range(-rangoLimite.x, rangoLimite.x),
                            Random.Range(-rangoLimite.y, rangoLimite.y), Random.Range(-rangoLimite.z, rangoLimite.z));
            abejas[i] = Instantiate(beePrefab, pos, Quaternion.identity);
        }

        ControladorFlock = this;
        colmenaPos = this.transform.position;

    }


}