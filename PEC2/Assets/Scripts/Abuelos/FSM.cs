using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class FSM : MonoBehaviour
{
    private NavMeshAgent agent;

    private WaitForSeconds wait = new WaitForSeconds(0.05f); 
    delegate IEnumerator State();
    private State state;

    Vector3 posInit;
    Vector3 posBench;

    bool isWandering = false;
    bool isWalking = false;
    bool isDetectBench = false;
    string nameLastBech;


    // En este met se crean los objetos que representan cada uno de los tres estados
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        nameLastBech = null;
    }

    // Aquí se indica cuál es el estado inicial
    IEnumerator Start()
    {
        yield return wait;

        state = Wander;

        while (enabled)
            yield return StartCoroutine(state());

    }
    // En cada fotograma se comprueba si hay que cambiar a otro estado
    void Update()
    {

        if (!isDetectBench)
        {

            if (isWandering == false)
            {
                StartCoroutine(Wander());
            }
            if (isWalking)
            {
                InsertPosition();
                OnAnimatorMove();

                isWalking = false;
            }

        }

    }

    void OnAnimatorMove()
    {
        // Update position to agent position
        transform.position = agent.nextPosition;
    }

    private void OnTriggerExit(Collider other)
    {
        if (isWalking)
        {
            if (!agent.isStopped)
            {
                isDetectBench = false;
  //              Debug.Log("Salgo del collider del Banco " + other.gameObject.name);

            } 
        }


    }


    //Si colisiona con el radio de un banco 
    private void OnTriggerEnter(Collider other)
    {

        if (!other.gameObject.name.Equals(nameLastBech))
        {

            if (!isDetectBench)
            {
                posBench = other.gameObject.transform.position;
                posInit = new Vector3(transform.position.x + 2.5f, transform.position.y, transform.position.z + 0.5f);

                isDetectBench = true;
                isWalking = false;

                if (agent.remainingDistance > 0.5f)
                {
                    agent.SetDestination(posBench);
 //                   Debug.Log("Cambio el destino al banco " + posBench + " agent.isStopped" + agent.isStopped);

                }

  //              Debug.Log(gameObject.name + ": Detecto el banco, Voy a sentarme en  " + agent.pathEndPosition + " mi posicion es: " + transform.position);

                state = Idle;

            }

        }
        else
        {
//            Debug.Log("El nombre del nuevo banco al que me dirijo" + nameLastBech);
            nameLastBech = other.gameObject.name;

        }

    }



    void InsertPosition()
    {

        if (agent.remainingDistance < 0.5f)
        {

            Vector3 localTarget = UnityEngine.Random.insideUnitCircle * Random.Range(2, 4);
            localTarget += new Vector3(0, 0, 0.5f);
            Vector3 worldTarget = transform.TransformPoint(localTarget);
            worldTarget.y = 0f;

            agent.SetDestination(worldTarget);
        }
    }


    IEnumerator Wander()
    {
        //     Debug.Log("*********** Wander ****************");

        isWandering = true;
        isWalking = true;

        yield return new WaitForSeconds(Random.Range(0, 1));

        isWalking = false;
        isWandering = false;

    }


    IEnumerator Idle()
    {
//        Debug.Log("*********** Idle ****************");

        //Dejo un tiempo para que el agente vaya a la posición del banco a sentarse
        yield return new WaitForSeconds(2);

        state = WaitInBench;

    }

    IEnumerator WaitInBench()
    {

      if (Vector3.Distance(transform.position, posBench) < 1.1f)
        {
  //      Debug.Log(gameObject.name+": Ahh! Estoy cansado, voy a esperar 5 segundos en el Banco." );

        yield return new WaitForSeconds(5);

        state = ExitTheBench;
        agent.SetDestination(posInit);
//        Debug.Log("Voy al " + agent.nextPosition);

        agent.isStopped = false;
        isWandering = false;
        isWalking = true;
        isDetectBench = false;
        }

    }

    IEnumerator ExitTheBench()
    {

        if (Vector3.Distance(transform.position, posBench) > 3.0f)
        {

            InsertPosition();
            state = Wander;
            yield return wait;
        }

    }

}
