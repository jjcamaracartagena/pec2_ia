using UnityEngine;
using UnityEngine.AI;

public class Patrol : MonoBehaviour
{

    public Transform[] points;

    private int destPoint = 0;
    private NavMeshAgent runner;

    void Start()
    {
        runner = GetComponent<NavMeshAgent>();
        runner.autoBraking = false; // Se deshabilida el auto-braking 

        GotoNextPoint();
    }

    void Update()
    {
  
        if (!runner.pathPending && runner.remainingDistance < 0.5f)
            GotoNextPoint();
    }

    void GotoNextPoint()
    {
        // Se asegura que haya algún WayPoint 
        if (points.Length == 0)
            return;

        // Se le indica el agente que vaya al actual WayPoint
        runner.destination = points[destPoint].position;

        destPoint = (destPoint + 1) % points.Length;
    }

}