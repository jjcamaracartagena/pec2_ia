using UnityEngine;
using UnityEngine.AI;

public class Pursue : MonoBehaviour
{

  public GameObject target;
    public float speedFactor;
    
    private NavMeshAgent runner;
    private Vector3 targetDir;
    private float lookAhead;


    void Start()
    {
        runner = GetComponent<NavMeshAgent>();
     

    }

    void Update()
    {

        if (Vector3.Distance(target.transform.position, transform.position) < 0.5f) return;

        targetDir = target.transform.position - transform.position;
        lookAhead = targetDir.magnitude / speedFactor;

        Seek(target.transform.position + target.transform.forward * lookAhead);
       
    }


    void Seek(Vector3 pos)
    {
        runner.destination = pos;
    }
}
