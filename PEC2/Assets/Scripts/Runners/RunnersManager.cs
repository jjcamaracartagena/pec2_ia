
using UnityEngine;

public class RunnersManager : MonoBehaviour
{
    public GameObject runPrefab;
    public GameObject ghostTarget;

    public int numRunners = 5;

    void Start()
    {
        GameObject[] allRunners = new GameObject[numRunners];


        for (int i = 0; i < numRunners; ++i)
        {

            //Se instancia el Runner de forma aleatoria en una posición, sentido y velocidad aleatoria
            Vector3 pos = this.transform.position + new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, 20));
            Quaternion orientation = Quaternion.Euler(Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f));

            allRunners[i] = Instantiate(runPrefab, pos, orientation);
            allRunners[i].GetComponent<Pursue>().target = ghostTarget;
            allRunners[i].GetComponent<Pursue>().speedFactor = Random.Range(20, 100);


        }

    }

}
